#pragma once

#include <QWidget>

class MainMenuScene : public QWidget
{
	Q_OBJECT

public:
	explicit MainMenuScene(double score, QWidget * parent = nullptr);
	~MainMenuScene();

signals:
	void playGameRequest();
	void exitRequest();
};

