#pragma once

#include <QWidget>

class GameOverScene : public QWidget
{
	Q_OBJECT

public:
	explicit GameOverScene(double score, double highestScore, QWidget * parent = nullptr);
	~GameOverScene();

signals:
	void playGameRequest();
	void backToMenuRequest();
};
