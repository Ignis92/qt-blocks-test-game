#include "MainMenuScene.h"

#include <QDebug>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>

#include "Definitions.h"

MainMenuScene::MainMenuScene(double score, QWidget * parent) : QWidget(parent)
{
	// Create title (top) layout
	auto titleLabel = new QLabel(tr("Blocks Game"));
	titleLabel->setAlignment(Qt::AlignHCenter);

	// Create menu button (middle) layout
	auto spacerMSx = new HSPACER;
	auto playButton = new QPushButton(tr("Play"));
	auto spacerMDx = new HSPACER;
	auto middleLayout = new QHBoxLayout();
	middleLayout->addItem(spacerMSx);
	middleLayout->addWidget(playButton);
	middleLayout->addItem(spacerMDx);

	// Create low menu layout
	auto spacerMLSx = new HSPACER;
	auto scoreLabel = new QLabel(tr("Highest score: ") + QString::number(score));
	auto spacerMLDx = new HSPACER;
	auto middleLowLayout = new QHBoxLayout();
	middleLowLayout->addItem(spacerMLSx);
	middleLowLayout->addWidget(scoreLabel);
	middleLowLayout->addItem(spacerMLDx);

	// Create bottom (exit) layout
	auto spacerB = new HSPACER;
	auto exitButton = new QPushButton();
	exitButton->setIcon(QIcon(":/icons/Resources/exitButton.png"));
	auto bottomLayout = new QHBoxLayout();
	bottomLayout->addItem(spacerB);
	bottomLayout->addWidget(exitButton);

	// Finalize layout
	auto mainLayout = new QVBoxLayout();
	auto spacerH = new VSPACER;
	auto spacerL = new VSPACER;
	mainLayout->addWidget(titleLabel);
	mainLayout->addItem(spacerH);
	mainLayout->addLayout(middleLayout);
	mainLayout->addItem(spacerL);
	mainLayout->addLayout(middleLowLayout);
	mainLayout->addLayout(bottomLayout);

	setLayout(mainLayout);

	// Connections
	connect(playButton, &QPushButton::clicked, this, [&]() { emit playGameRequest(); });
	connect(exitButton, &QPushButton::clicked, this, [&]() { emit exitRequest(); });
}

MainMenuScene::~MainMenuScene() {}
