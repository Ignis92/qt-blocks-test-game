#include "GameScene.h"

#include <QDebug>
#include <QHBoxLayout>
#include <QLabel>
#include <QTime>
#include <QTimer>
#include <QtMath>

#include "Block.h"
#include "Definitions.h"

GameScene::GameScene(QWidget * parent)
	: QWidget(parent), m_score(0), m_gameOverTimer(new QTimer(this)),
	  m_grid(NUM_ROW, QVector<Block *>(NUM_COL))
{
	// Create top ui
	timeLabel = new QLabel();
	auto spacerT = new HSPACER;
	scoreLabel = new QLabel(tr("Score: ") + QString::number(m_score));
	auto topLayout = new QHBoxLayout();
	topLayout->addWidget(timeLabel);
	topLayout->addItem(spacerT);
	topLayout->addWidget(scoreLabel);

	// Create central grid
	auto spacerMSx = new HSPACER;
	auto grid = new QGridLayout();
	for (int i = 0; i < m_grid.size(); ++i) {
		for (int j = 0; j < m_grid[0].size(); ++j) {
			auto block = new Block(i, j);
			grid->addWidget(block, i, j);
			// We also register all block
			m_grid[i][j] = block;
			connect(block, &Block::clicked, this, &GameScene::blockClicked);
		}
	}
	auto spacerMDx = new HSPACER;
	auto centralLayout = new QHBoxLayout();
	centralLayout->addItem(spacerMSx);
	centralLayout->addLayout(grid);
	centralLayout->addItem(spacerMDx);

	// Finalize layout
	auto spacerH = new VSPACER;
	auto spacerL = new VSPACER;
	auto mainLayout = new QVBoxLayout();
	mainLayout->addLayout(topLayout);
	mainLayout->addItem(spacerH);
	mainLayout->addLayout(centralLayout);
	mainLayout->addItem(spacerL);

	setLayout(mainLayout);

	// Timer setup
	m_gameOverTimer->setSingleShot(true);
	connect(m_gameOverTimer, &QTimer::timeout, this, [&]() { emit gameOverRequest(m_score); });
	m_gameOverTimer->start(COUNTDOWN_START * 1000);
	updateTime();

	auto labelTimer = new QTimer();
	connect(labelTimer, &QTimer::timeout, this, &GameScene::updateTime);
	labelTimer->start(100);

	checkPlayable();
}

GameScene::~GameScene() {}

void GameScene::blockClicked(int row, int column, BlockType type)
{
	int exploded;

	switch (type) {
	case BlockType::Bomb:
		qDebug() << "Bomb clicked";
		exploded = bombClicked(row, column);
		break;
	case BlockType::Stripe:
		qDebug() << "Stripe clicked";
		exploded = stripeClicked(row);
		break;
	default:
		qDebug() << "Colored block clicked";
		exploded = coloredClicked(row, column, type);
	}

	if (exploded) {
		m_score += (exploded - 1) * 80 + qPow(static_cast<double>(exploded - 2) / 5, 2);
		scoreLabel->setText(tr("Score: ") + QString::number(m_score));
		int timeToAdd = 10 + qPow(static_cast<double>(exploded - 2) / 3, 2) * 20;
		auto actualTime = m_gameOverTimer->remainingTime() + timeToAdd * 1000;
		m_gameOverTimer->start(actualTime);
	}

	checkPlayable();
}

void GameScene::updateTime()
{
	QTime time(0, 0, 0, 0);
	time = time.addMSecs(m_gameOverTimer->remainingTime());
	timeLabel->setText("Time left: " + time.toString());
}

int GameScene::bombClicked(int row, int column)
{
	int blockCounter = 0;
	m_grid[row][column]->destroy();
	++blockCounter;
	if (row > 0) {
		m_grid[row - 1][column]->destroy();
		++blockCounter;
		if (column > 0) {
			m_grid[row - 1][column - 1]->destroy();
			++blockCounter;
		}
		if (column < m_grid[0].size() - 1) {
			m_grid[row - 1][column + 1]->destroy();
			++blockCounter;
		}
	}
	if (row < m_grid.size() - 1) {
		m_grid[row + 1][column]->destroy();
		++blockCounter;
		if (column > 0) {
			m_grid[row + 1][column - 1]->destroy();
			++blockCounter;
		}
		if (column < m_grid[0].size() - 1) {
			m_grid[row + 1][column + 1]->destroy();
			++blockCounter;
		}
	}
	if (column > 0) {
		m_grid[row][column - 1]->destroy();
		++blockCounter;
	}
	if (column < m_grid[0].size() - 1) {
		m_grid[row][column + 1]->destroy();
		++blockCounter;
	}

	return blockCounter;
}

int GameScene::stripeClicked(int row)
{
	for (int i = 0; i < m_grid[0].size(); ++i)
		m_grid[row][i]->destroy();
	return m_grid[0].size();
}

int GameScene::coloredClicked(int row, int column, BlockType type)
{
	if (validNeighbour(row, column, type).size()) {
		QSet<QPair<int, int>> toBeDestroyed, newSet;
		toBeDestroyed.insert(QPair<int, int>(row, column));
		newSet = toBeDestroyed;
		while (newSet.size()) {
			QSet<QPair<int, int>> temp;
			for (auto const & elem : newSet) {
				temp.unite(validNeighbour(elem.first, elem.second, type));
			}
			temp.subtract(toBeDestroyed);
			temp.subtract(newSet);
			toBeDestroyed.unite(newSet);
			newSet = temp;
			qDebug() << "New set:" << newSet;
		}

		for (auto & elem : toBeDestroyed)
			m_grid[elem.first][elem.second]->destroy();
		return toBeDestroyed.size();
	} else {
		qDebug() << "Invalid colored block clicked";
		return 0;
	}
}

void GameScene::checkPlayable()
{
	for (int i = 0; i < m_grid.size(); ++i) {
		for (int j = 0; j < m_grid[0].size(); ++j) {
			if (m_grid[i][j]->type() == BlockType::Bomb || m_grid[i][j]->type() == BlockType::Stripe)
				return;
			else if (validNeighbour(i, j, m_grid[i][j]->type()).size())
				return;
		}
	}

	emit gameOverRequest(m_score);
}

QSet<QPair<int, int>> GameScene::validNeighbour(int row, int column, BlockType type)
{
	QSet<QPair<int, int>> validNeighbours;
	if (row > 0 && m_grid[row - 1][column]->type() == type)
		validNeighbours.insert(QPair<int, int>(row - 1, column));
	if (column > 0 && m_grid[row][column - 1]->type() == type)
		validNeighbours.insert(QPair<int, int>(row, column - 1));
	if (row < m_grid.size() - 1 && m_grid[row + 1][column]->type() == type)
		validNeighbours.insert(QPair<int, int>(row + 1, column));
	if (column < m_grid[0].size() - 1 && m_grid[row][column + 1]->type() == type)
		validNeighbours.insert(QPair<int, int>(row, column + 1));

	return validNeighbours;
}
