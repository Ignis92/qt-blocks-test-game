#include "GameOverScene.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>

#include "Definitions.h"

GameOverScene::GameOverScene(double score, double highestScore, QWidget * parent) : QWidget(parent)
{
	auto spacerC1 = new VSPACER;
	auto scoreLabel = new QLabel("Final score: " + QString::number(score));
	auto recordLabel = new QLabel("Highest score: " + QString::number(highestScore));
	auto spacerC2 = new VSPACER;
	auto menuButton = new QPushButton("Back to menu");
	auto playButton = new QPushButton("Play again");
	auto buttonBox = new QHBoxLayout();
	buttonBox->addWidget(menuButton);
	buttonBox->addWidget(playButton);
	auto spacerC3 = new VSPACER;
	auto centralLayout = new QVBoxLayout();
	centralLayout->addItem(spacerC1);
	centralLayout->addWidget(scoreLabel);
	centralLayout->addWidget(recordLabel);
	centralLayout->addItem(spacerC2);
	centralLayout->addLayout(buttonBox);
	centralLayout->addItem(spacerC3);

	auto spacerSx = new HSPACER;
	auto spacerDx = new HSPACER;
	auto mainLayout = new QHBoxLayout();
	mainLayout->addItem(spacerSx);
	mainLayout->addLayout(centralLayout);
	mainLayout->addItem(spacerDx);
	setLayout(mainLayout);

	connect(menuButton, &QPushButton::clicked, this, [&]() { emit backToMenuRequest(); });
	connect(playButton, &QPushButton::clicked, this, [&]() { emit playGameRequest(); });
}

GameOverScene::~GameOverScene() {}
