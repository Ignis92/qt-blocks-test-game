#include "Block.h"

#include <QDebug>
#include <QPainter>
#include <QRandomGenerator>

Block::Block(int row, int column, QWidget * parent) : QWidget(parent), m_row(row), m_column(column)
{
	setFixedSize(30, 30);
	create();
}

Block::~Block() {}

BlockType Block::type() const
{
	return m_type;
}

void Block::create()
{
	if (QRandomGenerator::global()->bounded(0, 20)) {
		m_type = static_cast<BlockType>(QRandomGenerator::global()->bounded(0, 6));
	} else {
		m_type = static_cast<BlockType>(QRandomGenerator::global()->bounded(6, 8));
	}
}

void Block::destroy()
{
	m_type = BlockType::Nothing;
	update();
	create();
	update();
}

void Block::paintEvent(QPaintEvent * event)
{
	QPainter painter(this);
	QColor color;

	switch (m_type) {
	case BlockType::Red:
		color = Qt::red;
		break;
	case BlockType::Blue:
		color = Qt::blue;
		break;
	case BlockType::Green:
		color = Qt::green;
		break;
	case BlockType::Yellow:
		color = Qt::yellow;
		break;
	case BlockType::Violet:
		color = QColor::fromRgb(238, 130, 238);
		break;
	case BlockType::Orange:
		color = QColor::fromRgb(255, 165, 0);
		break;
	case BlockType::Bomb:
	case BlockType::Stripe:
		color = Qt::cyan;
		break;
	case BlockType::Nothing:
		color = QColor::fromRgb(32, 48, 10, 0);
		break;
	default:
		color = Qt::black;
		qDebug() << "Unrecognized type of block";
	}

	painter.fillRect(rect(), color);

	if (m_type == BlockType::Bomb) {
		QPixmap icon = QPixmap(":/icons/Resources/bomb.png");
		painter.drawPixmap(rect(), icon);
	}

	if (m_type == BlockType::Stripe) {
		painter.fillRect(QRect(0, rect().height() / 3, rect().width(), rect().height() / 3),
						 Qt::black);
	}
}

void Block::mouseReleaseEvent(QMouseEvent * event)
{
	if (m_type != BlockType::Nothing) {
		qDebug() << "The block at position " << m_row << ", " << m_column << " was pressed";
		emit clicked(m_row, m_column, m_type);
	} else {
		qDebug() << "The block at position " << m_row << ", " << m_column << " is inactive";
	}
}
