#pragma once

#include <QMainWindow>

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow() override;

protected:
	void closeEvent(QCloseEvent * event) override;

private slots:
	void changeToGameScene();
	void changeToGameOver(double score);
	void changeToMenu();
	void exitGame();

private:
	void loadScore();

	double m_highestScore;
};
