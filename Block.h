#pragma once

#include <QWidget>

enum class BlockType { Red, Blue, Green, Yellow, Violet, Orange, Bomb, Stripe, Nothing };

class Block : public QWidget
{
	Q_OBJECT

public:
	explicit Block(int row, int column, QWidget * parent = nullptr);
	~Block() override;

	BlockType type() const;
	void create();
	void destroy();

protected:
	void paintEvent(QPaintEvent * event) override;
	void mouseReleaseEvent(QMouseEvent * event) override;

signals:
	void clicked(int row, int column, BlockType type);

private:
	BlockType m_type;
	const int m_row;
	const int m_column;
};
