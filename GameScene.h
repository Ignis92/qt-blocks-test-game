#pragma once

#include <QWidget>

#include "Block.h"

class QLabel;
class QTimer;

class GameScene : public QWidget
{
	Q_OBJECT

public:
	explicit GameScene(QWidget * parent = nullptr);
	~GameScene();

signals:
	void gameOverRequest(double score);

private slots:
	void blockClicked(int row, int column, BlockType type);
	void updateTime();

private:
	int bombClicked(int row, int column);
	int stripeClicked(int row);
	int coloredClicked(int row, int column, BlockType type);
	void checkPlayable();

	QSet<QPair<int, int>> validNeighbour(int row, int column, BlockType type);

	QLabel * timeLabel;
	QLabel * scoreLabel;

	double m_score;
	QTimer * m_gameOverTimer;
	QVector<QVector<Block *>> m_grid;
};

