#include "MainWindow.h"

#include <QApplication>
#include <QMessageBox>
#include <QSettings>

#include "GameOverScene.h"
#include "GameScene.h"
#include "MainMenuScene.h"

MainWindow::MainWindow(QWidget * parent) : QMainWindow(parent)
{
	setFixedSize(640, 480);
	loadScore();

	auto menu = new MainMenuScene(m_highestScore);
	setCentralWidget(menu);

	connect(menu, &MainMenuScene::playGameRequest, this, &MainWindow::changeToGameScene);
	connect(menu, &MainMenuScene::exitRequest, this, &MainWindow::exitGame);
}

MainWindow::~MainWindow() {}

void MainWindow::closeEvent(QCloseEvent * event)
{
	exitGame();
}

void MainWindow::changeToGameScene()
{
	auto game = new GameScene();
	connect(game, &GameScene::gameOverRequest, this, &MainWindow::changeToGameOver);
	auto previousScene = centralWidget();
	setCentralWidget(game);
	previousScene->deleteLater();
}

void MainWindow::changeToGameOver(double score)
{
	auto over = new GameOverScene(score, m_highestScore);
	connect(over, &GameOverScene::playGameRequest, this, &MainWindow::changeToGameScene);
	connect(over, &GameOverScene::backToMenuRequest, this, &MainWindow::changeToMenu);
	auto previousScene = centralWidget();
	setCentralWidget(over);
	previousScene->deleteLater();

	if (score > m_highestScore)
		m_highestScore = score;
}

void MainWindow::changeToMenu()
{
	auto menu = new MainMenuScene(m_highestScore);
	connect(menu, &MainMenuScene::playGameRequest, this, &MainWindow::changeToGameScene);
	connect(menu, &MainMenuScene::exitRequest, this, &MainWindow::exitGame);
	auto previousScene = centralWidget();
	setCentralWidget(menu);
	previousScene->deleteLater();
}

void MainWindow::exitGame()
{
	auto answer = QMessageBox::question(this,
										tr("Exit request"),
										tr("Are you sure you want to exit the game?"));
	if (answer == QMessageBox::Yes) {
		QApplication::quit();
		QSettings settings("redbit", "blocks_game");
		settings.setValue("highest_score", m_highestScore);
	}
}

void MainWindow::loadScore()
{
	QSettings settings("redbit", "blocks_game");
	if (settings.value("highest_score") == QVariant())
		m_highestScore = 0;
	else
		m_highestScore = settings.value("highest_score").toReal();
}
