#pragma once

#define NUM_ROW 4
#define NUM_COL 4
#define COUNTDOWN_START 120
#define HSPACER QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum)
#define VSPACER QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding)
